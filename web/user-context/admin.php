<?php
session_start();
$role = $_SESSION['role'] ? $_SESSION['role'] : 'anonymous';

header('Vary: X-User-Context-Hash');
header('Cache-Control: max-age=0');
header('X-Reverse-Proxy-TTL: 3600');

if ($role == 'admin') {
    $body = '<h1>The admin panel...</h1>';
} else {
    header('HTTP/1.1 403');
    $body = '<h1>Only admins may see this</h1>';
}

require('template.php');
