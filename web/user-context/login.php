<?php
session_start();

header('Cache-Control: no-cache, s-maxage=0, max-age=0');

if ('POST' === $_SERVER['REQUEST_METHOD']) {
    session_regenerate_id(true);
    $_SESSION['username'] = $_POST['username'];
    $_SESSION['role'] = $_POST['role'];
    header('Location: http://varnish.lo' . dirname($_SERVER['PHP_SELF']));
    die;
}

$role = $_SESSION['role'] ? $_SESSION['role'] : 'anonymous';
$body = '<form method="post">
            <label for="name">Username</label>:
            <input id="name" name="username" value="'.$_SESSION['username'].'"><br/>
            <label for="role">Role</label>:
            <select id="role" name="role">
                <option value="user"' . ('user' == $role ? ' selected' : '') . '>User</option>
                <option value="admin"' . ('admin' == $role ? ' selected' : '') . '>Admin</option>
            </select><br/>
            <input type="submit"/>
        </form>
';

require('template.php');
