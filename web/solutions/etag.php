<?php

/*
 * Request this page with an ETag check:
 *
 * curl -H "If-None-Match: abc" -sD - varnish.lo/etag.php
 */

header('Cache-Control: s-maxage=30');
header('ETag: abc');

echo date("Y-m-d H:i:s") . "\n";
