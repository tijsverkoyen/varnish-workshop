<?php

/*
 * Request this page and get it from cache until the age reaches 10 seconds:
 *
 * curl -sD - varnish.lo/expiration.php
 */

header('Cache-Control: public, s-maxage=10');

echo date("Y-m-d H:i:s") . "\n";
