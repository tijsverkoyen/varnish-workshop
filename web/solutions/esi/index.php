<?php
// tell varnish to do ESI
header('Surrogate-Control: ESI/1.0');
// tell varnish to cache this, but not other proxies
header('X-Reverse-Proxy-TTL: 500');

// Note: ESI by default only operates on HTML documents.
?>
<html><body>
<?php
sleep(1);
echo 'Main body: ' . date('Y-m-d H:i:s') . "\n";
?>
<esi:include src="fragment.php" />
</body></html>
