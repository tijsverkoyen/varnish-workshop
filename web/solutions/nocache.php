<?php

/*
 * Request this page and see it is not cached:
 *
 * curl -sD - varnish.lo/nocache.php
 */

header('Cache-Control: no-cache, s-maxage=0, max-age=0');

echo date("Y-m-d H:i:s") . "\n";
