<?php
header('Cache-Control: no-cache');
?>
<html>
    <head>
        <title>Varnish playground vagrant box</title>
    </head>
    <body>
        <h1>The varnish box works!</h1>
        <p>If you see this page, the vagrant setup has worked correctly.</p>

        <ul>
        <?php
            $ignore = array('index.php', 'user_context_hash.php');
            foreach (glob(__DIR__ . '/*') as $file) {
                $file = substr($file, strlen(__DIR__) + 1);
                if (in_array($file, $ignore)) continue;
                echo "<li><a href='$file'>$file</a></li>";
            }
        ?>
        </ul>
        <p><?php echo date("Y-m-d H:i:s"); ?></p>
    </body>
</html>
