<?php
require('../../vendor/autoload.php');

use FOS\HttpCache\CacheInvalidator;
use FOS\HttpCache\ProxyClient\Varnish;

$client = new Varnish(array('127.0.0.1'), 'varnish.lo');
$invalidator = new CacheInvalidator($client);

if ('POST' !== $_SERVER['REQUEST_METHOD'] || !isset($_POST['method'])) {
    die('Go to ./ and use the form');
}

switch ($_POST['method']) {
    case 'Purge':
        $invalidator->invalidatePath('/invalidation/');
        break;
    case 'Refresh':
        $invalidator->refreshPath('/invalidation/');
        break;
    case 'Ban':
        $invalidator->invalidateRegex('^/invalidation/.*');
        break;
    default:
        die('Unexpected invalidation method');
}
$invalidator->flush();

$location = 'http://varnish.lo/invalidation/';
if ($_SERVER['QUERY_STRING']) {
    $location .= '?'.$_SERVER['QUERY_STRING'];
}
header('Location: '.$location);
