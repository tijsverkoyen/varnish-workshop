<?php

/*
 * Add a last modified timestamp to be albe to request this page with a modification time check:
 *
 * curl -H "If-Modified-Since: <last timestamp>" -sD - varnish.lo/last-modified.php
 *
 * Note: The timestamp must not be in the future or you will see 200 and content instead of 304.
 */

header('Cache-Control: s-maxage=300');

$data = date("Y-m-d H:i:s");

// note: gmdate('r') outputs the timezone as +0000 instead of GMT, making varnish 3 unhappy.
// format the date with gmdate('D, j M Y H:i:00 T')
// something is missing here

echo $data . "\n";
