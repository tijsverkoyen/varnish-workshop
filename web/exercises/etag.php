<?php

/*
 * Use the static ETag "abc" for this content:
 *
 * curl -H "If-None-Match: abc" -sD - varnish.lo/etag.php
 */

header('Cache-Control: s-maxage=3600');
// something is missing here

echo date("Y-m-d H:i:s") . "\n";
