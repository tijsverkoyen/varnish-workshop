<?php

/*
 * Tell varnish that this page depends on the Accept header:
 *
 * curl -H "Accept: application/json" -sD - varnish.lo/content-negotiation.php
 * curl -sD - varnish.lo/content-negotiation.php
 */

header('Cache-Control: s-maxage=60');
// something is missing here!

$data = date("Y-m-d H:i:s");
if (false !== strpos($_SERVER['HTTP_ACCEPT'], 'application/json')) {
    header('Content-Type: application/json');
    echo json_encode(array('date' => $data)) . "\n";
    exit;
}

header('Content-Type: text/plain');
// fall back to text/plain
echo $data . "\n";
