<?php

/*
 * Move the non-cached part to an ESI fragment, make this main page cached for 600 seconds.
 */

// no caching because of fragment part only
header('Cache-Control: no-cache, s-maxage=0');

// Note: ESI by default only operates on HTML documents.
?>
<html><body>
<?php
// main part: takes time, may be 600 seconds old
sleep(1);
echo 'Main body: ' . date('Y-m-d H:i:s') . "\n";

// must not be cached, is fast to do. move this to an ESI fragment
echo 'Fragment part: ' . date("Y-m-d H:i:s") . "\n";
?>
</body></html>
