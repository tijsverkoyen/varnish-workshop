<?php

/*
 * Make this page cached by Varnish for 30 seconds but keep s-maxage at 0
 *
 * curl -sD - varnish.lo/custom-ttl.php
 */

header('Cache-Control: public, s-maxage=0, max-age=0');
// something is missing here

echo date("Y-m-d H:i:s") . "\n";
