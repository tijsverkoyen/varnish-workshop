# install necessary software
%w(
varnish
vim
apache2
php5
libapache2-mod-php5
php5-cli
php5-curl
php5-sqlite
php5-pgsql
php5-intl
apt-transport-https
).each { | pkg | package pkg }

# copy virtual hosts file from template
template "/etc/apache2/sites-available/default" do
  mode "0644"
  source "vhost.conf.erb"
  #notifies :reload, "service[apache2]"
end

bash "modify timezone" do
  code <<-EOC
    sudo cp /etc/timezone /etc/timezone_old
    echo "Europe/Zurich" > /etc/timezone
    dpkg-reconfigure -f noninteractive tzdata
  EOC

  creates "/etc/timezone_old"
end

# set the timezone for php
execute "date.timezone = Europe/Zurich in php.ini?" do
  user "root"
  not_if "test -f /etc/php5/conf.d/10-timezone.ini"
  command "echo 'date.timezone = Europe/Zurich' > /etc/php5/conf.d/10-timezone.ini"
end

bash "have apache listen on port 8080" do
  user "root"
  not_if "grep 'Listen 8080' /etc/apache2/ports.conf"
  code <<-EOH
    sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf
    sed -i 's/NameVirtualHost \\*:80/NameVirtualHost *:8080/g' /etc/apache2/ports.conf
  EOH
  notifies :restart, "service[apache2]"
end

execute "have varnish listen on port 80" do
  user "root"
  not_if "grep ' :80 ' /etc/default/varnish"
  command "sed -i 's/-a :6081 /-a :80 /g' /etc/default/varnish"
  notifies :restart, "service[varnish]"
end

bash "link varnish configuration" do
  user "root"
  not_if "test -L /etc/varnish/default.vcl"
  code <<-EOH
      rm /etc/varnish/default.vcl
      ln -s /vagrant/config/varnish/dev.vcl /etc/varnish/default.vcl
      ln -s /vagrant/config/varnish/* /etc/varnish/
  EOH
  notifies :restart, "service[varnish]"
end

# Start and enable Apache
service "apache2" do
  supports :restart => true, :reload => true, :status => true
  action [ :enable, :start ]
end

# Start Varnish
service "varnish" do
  supports :restart => true, :status => true
  action [ :enable, :start ]
end
