# include "custom-ttl.vcl";
include "esi.vcl";
# include "invalidation.vcl";
# include "user-context.vcl";

sub vcl_fetch {
    // respect no-cache from backend. default for Varnish4
    if (beresp.http.cache-control ~ "(no-cache|no-store|private)"
        || beresp.http.pragma ~ "no-cache"
    ) {
            set beresp.ttl = 0s;
    }
}
