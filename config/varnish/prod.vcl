# entry point for production environment without debug information

backend default {
    .host = "127.0.0.1";
    .port = "8080";
}

include "common.vcl";
